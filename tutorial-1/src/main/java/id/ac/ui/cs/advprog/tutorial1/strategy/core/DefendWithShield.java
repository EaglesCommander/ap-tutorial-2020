package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    public String getType(){
        return "Shield";
    }
    public String defend(){
        return "Defend with shield";
    }
}
