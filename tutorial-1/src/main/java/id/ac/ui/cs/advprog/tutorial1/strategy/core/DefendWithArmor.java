package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    public String getType(){
        return "Armor";
    }
    public String defend(){
        return "Defend with armor";
    }
}
