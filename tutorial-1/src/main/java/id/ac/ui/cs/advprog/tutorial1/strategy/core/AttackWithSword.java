package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    public String getType(){
        return "Sword";
    }

    public String attack(){
            return "Attack with sword";
    }
}
