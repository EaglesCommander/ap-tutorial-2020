package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {

        public AgileAdventurer(Guild guild) {
                this.name = "Agile";
                this.guild = guild;
        }

        public void update(){
                String questType = this.guild.getQuestType();

                if (!(questType.equals("E"))) {
                        this.getQuests().add(this.guild.getQuest());
                }
        }
}
