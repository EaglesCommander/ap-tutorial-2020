package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
        public String name;
        public String role;
        public List<Member> subordinate = new ArrayList<Member>();

        public OrdinaryMember(String name, String role){
            this.name = name;
            this.role = role;

            if (name == ""){
                this.name = "null";
            }

            if (role == ""){
                this.role = "null";
            }
        }

        public String getName(){
            return this.name;
        }

        public String getRole(){
            return this.role;
        }

        public void addChildMember(Member member){
            return;
        }

        public void removeChildMember(Member member){
            return;
        }

        public List<Member> getChildMembers(){
            return this.subordinate;
        }
}
