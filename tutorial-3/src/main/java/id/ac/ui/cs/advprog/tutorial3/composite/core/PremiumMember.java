package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    public String name;
    public String role;
    public List<Member> subordinate = new ArrayList<Member>();

    public PremiumMember(String name, String role){
        this.name = name;
        this.role = role;

        if (name == ""){
            this.name = "null";
        }

        if (role == ""){
            this.role = "null";
        }
    }

    public String getName(){
        return this.name;
    }

    public String getRole(){
        return this.role;
    }

    public void addChildMember(Member member){
        List<Member> subordinateList = this.getChildMembers();

        if (subordinateList.size() >= 3 && !(this.getRole().equals("Master"))){
            return;
        }

        subordinateList.add(member);
    }

    public void removeChildMember(Member member){
        List<Member> subordinateList = this.getChildMembers();
        subordinateList.remove(member);
    }

    public List<Member> getChildMembers(){
        return this.subordinate;
    }
}
