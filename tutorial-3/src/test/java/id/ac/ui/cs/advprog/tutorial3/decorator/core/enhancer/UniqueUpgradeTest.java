package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Shield;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UniqueUpgradeTest {

    private UniqueUpgrade uniqueUpgrade;

    @BeforeEach
    public void setUp(){
        uniqueUpgrade = new UniqueUpgrade(new Shield());
    }

    @Test
    public void testMethodGetWeaponName(){
        assertEquals(uniqueUpgrade.getName(), "Shield");
    }

    @Test
    public void testMethodGetWeaponDescription(){
        assertEquals(uniqueUpgrade.getDescription(), "Unique Heater Shield");
    }

    @Test
    public void testMethodGetWeaponValue(){
        assertTrue(uniqueUpgrade.getWeaponValue() >= 20);
        assertTrue(uniqueUpgrade.getWeaponValue() <= 25);
    }
}
