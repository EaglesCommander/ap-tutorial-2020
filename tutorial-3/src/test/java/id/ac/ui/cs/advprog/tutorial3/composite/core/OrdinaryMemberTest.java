package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrdinaryMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new OrdinaryMember("Nico", "Waifu");
    }

    @Test
    public void testMethodGetName() {
        assertEquals(member.getName(), "Nico");
    }

    @Test
    public void testMethodGetRole() {
        assertEquals(member.getRole(), "Waifu");
    }

    @Test
    public void testMethodAddRemoveChildMemberDoNothing() {
        member.addChildMember(member);
        assertTrue(member.getChildMembers().size() == 0);
        member.removeChildMember(member);
        assertTrue(member.getChildMembers().size() == 0);
    }
}
