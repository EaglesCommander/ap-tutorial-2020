package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Longbow;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MagicUpgradeTest {


    private MagicUpgrade magicUpgrade;

    @BeforeEach
    public void setUp(){
        magicUpgrade = new MagicUpgrade(new Longbow());
    }

    @Test
    public void testMethodGetWeaponName(){
        assertEquals(magicUpgrade.getName(), "Longbow");
    }

    @Test
    public void testMethodGetWeaponDescription(){
        assertEquals(magicUpgrade.getDescription(), "Magic Big Longbow");
    }

    @Test
    public void testMethodGetWeaponValue(){
        assertTrue(magicUpgrade.getWeaponValue() >= 30);
        assertTrue(magicUpgrade.getWeaponValue() <= 35);
    }
}
