package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;
    private Member guildMember;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Nico", "Master");
        guildMember = new PremiumMember("Maki", "Backup");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        guild.addMember(guildMaster, guildMember);

        List<Member> memberList = guild.getMemberList();
        assertEquals(memberList.size(), 2);
    }

    @Test
    public void testMethodRemoveMember() {
        guild.addMember(guildMaster, guildMember);

        List<Member> memberList = guild.getMemberList();
        assertEquals(memberList.size(), 2);

        guild.removeMember(guildMaster, guildMember);

        memberList = guild.getMemberList();
        assertEquals(memberList.size(), 1);
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        guild.addMember(guildMaster, guildMember);

        Member returnedMember = guild.getMember("Maki", "Backup");

        assertEquals(returnedMember.getName(), "Maki");
        assertEquals(returnedMember.getRole(), "Backup");
    }
}
