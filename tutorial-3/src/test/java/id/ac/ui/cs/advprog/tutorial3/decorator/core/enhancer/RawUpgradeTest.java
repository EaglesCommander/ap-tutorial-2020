package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Longbow;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Shield;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RawUpgradeTest {

    private RawUpgrade rawUpgrade;

    @BeforeEach
    public void setUp(){
        rawUpgrade = new RawUpgrade(new Shield());
    }

    @Test
    public void testMethodGetWeaponName(){
        assertEquals(rawUpgrade.getName(), "Shield");
    }

    @Test
    public void testMethodGetDescription(){
        assertEquals(rawUpgrade.getDescription(), "Raw Heater Shield");
    }

    @Test
    public void testMethodGetWeaponValue(){
        assertTrue(rawUpgrade.getWeaponValue() >= 15);
        assertTrue(rawUpgrade.getWeaponValue() <= 20);
    }
}
