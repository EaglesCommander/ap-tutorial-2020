package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;
    private Member master;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Aqua", "Goddess");
        master = new PremiumMember("Nico", "Master");
    }

    @Test
    public void testMethodGetName()
    {
        assertEquals(this.member.getName(), "Aqua");
    }

    @Test
    public void testMethodGetRole() {
        assertEquals(this.member.getRole(), "Goddess");
    }

    @Test
    public void testMethodAddChildMember() {
        List<Member> child_list = this.member.getChildMembers();

        this.member.addChildMember(this.member);
        assertEquals(child_list.size(), 1);

        Member child_member = child_list.get(0);
        assertEquals(child_member.getName(), "Aqua");
    }

    @Test
    public void testMethodRemoveChildMember() {
        List<Member> child_list = member.getChildMembers();

        member.addChildMember(member);
        assertEquals(child_list.size(), 1);

        member.removeChildMember(member);
        assertEquals(child_list.size(), 0);
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        for (int i = 0; i < 4; i++){
            member.addChildMember(member);
        }

        List<Member> child_list = member.getChildMembers();
        assertEquals(child_list.size(), 3);
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        for (int i = 0; i < 4; i++){
            master.addChildMember(master);
        }

        List<Member> child_list = master.getChildMembers();
        assertEquals(child_list.size(), 4);
    }
}
