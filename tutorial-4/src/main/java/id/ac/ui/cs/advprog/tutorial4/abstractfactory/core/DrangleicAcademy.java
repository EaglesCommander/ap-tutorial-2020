package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.DrangleicArmory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;

public class DrangleicAcademy extends KnightAcademy {

    @Override
    public String getName() {
        return "Drangleic";
    }

    @Override
    protected Knight produceKnight(String type) {
        Knight knight = null;
        Armory armory = new DrangleicArmory();

        switch (type) {
            case "majestic":
                knight = new MajesticKnight(armory);
                knight.setName("Majestic Drangleic Knight");

                break;
            case "metal cluster":
                knight = new MetalClusterKnight(armory);
                knight.setName("Metal Cluster Drangleic Knight");

                break;
            case "synthetic":
                knight = new SyntheticKnight(armory);
                knight.setName("Synthetic Drangleic Knight");

                break;
        }

        return knight;
    }
}
