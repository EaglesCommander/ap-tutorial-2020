package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    private HolyGrail holyGrail;

    @BeforeEach
    public void setUp(){
        holyGrail = new HolyGrail();
    }

    @Test
    public void holyWishExistOnCreation(){
        assertNotNull(holyGrail.getHolyWish());
    }

    @Test
    public void onGetInstanceReturnedHolyWishIsSingleton(){
        HolyWish holyWish = HolyWish.getInstance();
        assertEquals(holyWish, holyGrail.getHolyWish());
    }

    @Test
    public void whenMakeAWishIsCalledItShouldReturnNewWish(){
        HolyWish holyWish = holyGrail.getHolyWish();
        holyGrail.makeAWish("I want to be the number 1 idol");
        assertEquals(holyWish.getWish(), "I want to be the number 1 idol");
    }
}
