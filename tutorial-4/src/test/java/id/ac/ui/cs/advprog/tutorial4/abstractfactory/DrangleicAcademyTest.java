package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class DrangleicAcademyTest {
    KnightAcademy drangleicAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        drangleicAcademy = new DrangleicAcademy();
        majesticKnight = drangleicAcademy.getKnight("majestic");
        metalClusterKnight = drangleicAcademy.getKnight("metal cluster");
        syntheticKnight = drangleicAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        assertTrue(majesticKnight instanceof MajesticKnight);
        assertTrue(metalClusterKnight instanceof MetalClusterKnight);
        assertTrue(syntheticKnight instanceof SyntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        assertEquals("Majestic Drangleic Knight", majesticKnight.getName());
        assertEquals("Metal Cluster Drangleic Knight", metalClusterKnight.getName());
        assertEquals("Synthetic Drangleic Knight", syntheticKnight.getName());
    }

    @Test
    public void checkKnightDescriptions() {
        assertEquals("METAL GEAR", majesticKnight.getArmor().getDescription());
        assertEquals("Tokusatsu Weeb", majesticKnight.getWeapon().getDescription());
        assertEquals("METAL GEAR", metalClusterKnight.getArmor().getDescription());
        assertEquals("Kakashi's famous jutsu", metalClusterKnight.getSkill().getDescription());
        assertEquals("Kakashi's famous jutsu", syntheticKnight.getSkill().getDescription());
        assertEquals("Tokusatsu Weeb", syntheticKnight.getWeapon().getDescription());
    }

    @Test
    public void checkAcademyName() {
        assertEquals("Drangleic", drangleicAcademy.getName());
    }

}
