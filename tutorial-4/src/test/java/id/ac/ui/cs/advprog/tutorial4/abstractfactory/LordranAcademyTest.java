package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class LordranAcademyTest {
    KnightAcademy lordranAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        lordranAcademy = new LordranAcademy();
        majesticKnight = lordranAcademy.getKnight("majestic");
        metalClusterKnight = lordranAcademy.getKnight("metal cluster");
        syntheticKnight = lordranAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        assertTrue(majesticKnight instanceof MajesticKnight);
        assertTrue(metalClusterKnight instanceof MetalClusterKnight);
        assertTrue(syntheticKnight instanceof SyntheticKnight);
    }

    @Test
    public void checkKnightNames() {
        assertEquals("Majestic Lordran Knight", majesticKnight.getName());
        assertEquals("Metal Cluster Lordran Knight", metalClusterKnight.getName());
        assertEquals("Synthetic Lordran Knight", syntheticKnight.getName());
    }

    @Test
    public void checkKnightDescriptions() {
        assertEquals("Divine Shield", majesticKnight.getArmor().getDescription());
        assertEquals("SHINY", majesticKnight.getWeapon().getDescription());
        assertEquals("Divine Shield", metalClusterKnight.getArmor().getDescription());
        assertEquals("SHINING", metalClusterKnight.getSkill().getDescription());
        assertEquals("SHINING", syntheticKnight.getSkill().getDescription());
        assertEquals("SHINY", syntheticKnight.getWeapon().getDescription());
    }

    @Test
    public void checkAcademyName() {
        assertEquals("Lordran", lordranAcademy.getName());
    }
}
