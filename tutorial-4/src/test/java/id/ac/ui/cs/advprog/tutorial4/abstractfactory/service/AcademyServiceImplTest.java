package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;

import java.util.*;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository;
    private AcademyService academyService;

    @BeforeEach
    public void setUp(){
        academyRepository = new AcademyRepository();
        academyService = new AcademyServiceImpl(academyRepository);
    }

    @Test
    public void onProduceShouldGenerateRightTypeOfKnight(){
        academyService.produceKnight("Lordran", "majestic");
        Knight knight = academyService.getKnight();

        assertEquals("Majestic Lordran Knight", knight.getName());
    }

    @Test
    public void onGetAcademyShouldReturnTheRightAcademies(){
        List<KnightAcademy> knightAcademies = academyService.getKnightAcademies();

        assertEquals("Lordran", knightAcademies.get(0).getName());
        assertEquals("Drangleic", knightAcademies.get(1).getName());
    }
}
