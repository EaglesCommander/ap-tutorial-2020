package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {

    private Map<String, Spell> spells;
    private Spell latestSpell;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        Spell curr_spell = this.spells.get(spellName);
        curr_spell.cast();
        this.latestSpell = curr_spell;
    }

    public void undoSpell() {
        if (this.latestSpell == null){
            return;
        }
        this.latestSpell.undo();
        this.latestSpell = null;
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
