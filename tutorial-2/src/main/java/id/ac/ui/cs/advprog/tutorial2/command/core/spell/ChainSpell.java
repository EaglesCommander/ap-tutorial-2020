package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    protected ArrayList<Spell> spell_list;

    public ChainSpell(ArrayList<Spell> spell_list){
        this.spell_list = spell_list;
    }

    @Override
    public void cast() {
        for (Spell spell : this.spell_list){
            spell.cast();
        }
    }

    @Override
    public void undo() {
        for (int i = this.spell_list.size()-1; i>=0; i--){
            spell_list.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
